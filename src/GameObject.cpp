#include "GameObject.h"
#include "ofApp.h"

GameObject::GameObject(ofImage p_sprite, ofVec2f p_position) {
	sprite = p_sprite;
	position = p_position;
}

void GameObject::setPosition(ofVec2f p_position) {
	position = p_position - ofVec2f(sprite.getWidth() / 2, sprite.getHeight() / 2);
}
void GameObject::translate(ofVec2f p_direction) {
	position = position + p_direction;
}

void GameObject::draw() {
	ofPushMatrix();
		ofTranslate(position);
		sprite.draw(0, 0);
	ofPopMatrix();
}

ofVec2f GameObject::getCenterPosition() {
	return ofVec2f(position.x + sprite.getWidth() / 2, position.y + sprite.getHeight() / 2);
}

GameObject::GameObject() {}
GameObject::~GameObject() {}
