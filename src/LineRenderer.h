#pragma once
#include <vector>
#include "ofGraphics.h"

class LineRenderer
{
public:
	vector<ofVec2f>* points;
	void setPoints(vector<ofVec2f>* p_points);
	void draw();
};